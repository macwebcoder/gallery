'use strict';

/**
 * @ngdoc directive
 * @name galleryApp.directive:galleryImage
 * @description
 * # galleryImage
 */
angular.module('galleryApp')
    .directive('galleryImage', function () {
        return {
            template: '',
            restrict: 'A',
            scope: {
                image: '='
            },
            link: function postLink(scope, element, attrs) {
                var img = angular.element('<img>');

                img.attr('src', scope.image.url).
                    attr('alt', scope.image.name).
                    addClass('img-thumbnail');

                element.append(img);
            }
        };
    });
