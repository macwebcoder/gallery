'use strict';

/**
 * @ngdoc function
 * @name galleryApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the galleryApp
 */
angular.module('galleryApp')
  .controller('MainCtrl', function ($scope, wiki) {

        $scope.keyword = '';
        $scope.images = [];
        $scope.search = function () {
            if (!$scope.keyword.length) {
                $scope.images = [];
                return;
            }

            wiki.setSearchPrefix($scope.keyword);
            wiki.getImages().then(function(data){
                $scope.images = data;
            });
        };

  });
