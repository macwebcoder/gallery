'use strict';

/**
 * @ngdoc service
 * @name galleryApp.wiki
 * @description
 * # wiki
 * Service in the galleryApp.
 */
angular.module('galleryApp')
    .service('wiki', function wiki($http, $q, underscore) {

        var acceptedMimeArray = [
                'image/jpeg',
                'image/png'
            ],
            resultsLimit = 50,
            propSeparator = '|',
            propertiesArray = ['dimensions', 'mime', 'url'],
            searchKeyword = '',
            apiUrl = 'http://en.wikipedia.org/w/api.php',
            setApiPromise,
            api;

        setApiPromise = function () {
            api = $http.jsonp(apiUrl, {
                params: {
                    action: 'query',
                    list: 'allimages',
                    ailimit: resultsLimit,
                    aifrom: searchKeyword,
                    aiprop: propertiesArray.join(propSeparator),
                    format: 'json',
                    callback: 'JSON_CALLBACK'
                }
            });
        };

        return {
            getImages: function () {
                return api.then(
                    function (response) {
                        var images = [];

                        // Use only accepted mime types of files
                        angular.forEach(response.data.query.allimages, function (img) {
                            if (underscore.indexOf(acceptedMimeArray, img.mime) !== -1) {
                                images.push(img);
                            }
                        });

                        return images;
                    },
                    function (response) {
                        return $q.reject(response.data);
                    }
                );
            },
            setResultLimit: function (limit) {
                limit = parseInt(limit, 10);
                if (limit <= 0) {
                    return false;
                }

                resultsLimit = limit;
                return true;
            },
            setSearchPrefix: function (keyword) {
                if (keyword === undefined || !keyword.length) {
                    return false;
                }
                searchKeyword = keyword;
                setApiPromise();
                return true;
            }
        };

    });
