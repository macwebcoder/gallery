# README #

## How do I get set up? ##

### Install dependencies ###
* Node.js with npm packages manager
* Grunt with grunt-cli
* Bower

### How to install application? ###
1. Checkout master repo from Bitbucket to local directory (eg. ~/Sites/gallery)
2. Go to application directory (cd ~/Sites/gallery)
3. Install required Node modules (sudo npm install)
4. Install Bower dependencies (bower install)

### How to build current version of application? ###
1. Go to application directory (cd ~/Sites/gallery)
2. Build with Grunt (grunt build)
3. Check "dist" directory for builded version of application

### How to preview application in local browser? ###
1. Go to application directory (cd ~/Sites/gallery)
2. Use Grunt serve option (grunt serve)