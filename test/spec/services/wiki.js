'use strict';

describe('Service: wiki', function () {

  // load the service's module
  beforeEach(module('galleryApp'));

  // instantiate service
  var wiki;
  beforeEach(inject(function (_wiki_) {
    wiki = _wiki_;
  }));

  it('should do something', function () {
    expect(!!wiki).toBe(true);
  });

});
